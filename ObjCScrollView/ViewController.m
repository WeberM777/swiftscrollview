//
//  ViewController.m
//  ObjCScrollView
//
//  Created by Martha A Canizles on 9/6/17.
//  Copyright © 2017 Martha A Canizales. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView* scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];//Initalizing scrollview memory and allocating, and setting frame
    scrollview.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height * 9);
    scrollview.backgroundColor = [UIColor blueColor];
    [scrollview setPagingEnabled:YES];
    [self.view addSubview:scrollview];
    
    UIView* view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height * 9)];
    view1.backgroundColor = [UIColor redColor];
    [scrollview addSubview:view1];
    
    UIView* view2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height * 8)];
    view2.backgroundColor = [UIColor purpleColor];
    [scrollview addSubview:view2];
    
    UIView* view3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height * 7)];
    view3.backgroundColor = [UIColor greenColor];
    [scrollview addSubview:view3];
    
    UIView* view4 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height * 6)];
    view4.backgroundColor = [UIColor yellowColor];
    [scrollview addSubview:view4];
    
    UIView* view5 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height * 5)];
    view5.backgroundColor = [UIColor blackColor];
    [scrollview addSubview:view5];
    
    UIView* view6 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height * 4)];
    view6.backgroundColor = [UIColor cyanColor];
    [scrollview addSubview:view6];
    
    UIView* view7 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height * 3)];
    view7.backgroundColor = [UIColor darkGrayColor];
    [scrollview addSubview:view7];
    
    UIView* view8 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height * 2)];
    view8.backgroundColor = [UIColor magentaColor];
    [scrollview addSubview:view8];
    
    UIView* view9 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height * 1)];
    view9.backgroundColor = [UIColor blueColor];
    [scrollview addSubview:view9];

    
    // Do any additional setup after loading the view, typically from a nib.
    
    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

