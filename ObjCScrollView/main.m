//
//  main.m
//  ObjCScrollView
//
//  Created by Martha A Rodriguez on 9/6/17.
//  Copyright © 2017 Martha A Canizales. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
